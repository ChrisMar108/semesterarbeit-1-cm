# Dokument-Zweck
Dieses Readme ist nur die Platzhalterung, weil eine eigene Struktur aufgebaut wird. <br>

Erklärung der Ordnerstruktur: 
- Dokumentation -> Hier kommen alle Dokumenationen der verschiedenen Tage sowie Arten (Zum Beispiel Namenskonvention). Der Anfang wird immer mit dem Datum vermerkt, wann an etwas gearbeitet wurde in Zeile 1

- Screenshots -> Hier kommen alle Bilder (Hauptsächlich Screenshots)

- Scripts -> Hierhin kommen die 2 PowerShell Scripts während bearbeitung sowie Updates (Allgemeine Bearbeitung)

Je nach Umständen kann es auch weitere Ordner geben, erklärungen kommen wenn Bedarf vorhanden.