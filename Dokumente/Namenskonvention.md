<h2><b>Namenskonvention</h2></b>
Die Namenskonventionen ergeben sich aus folgenden Kategorien:

<br>

<h3> <b>Identitäten </h3></b>

<br>

<h4> Definition: </h4> 
Sie haben das Privileg, Mitglieder von jeder Gruppe sein können (Ausnahmen sind die Lokalen und Domänenlokale Guppen ausserhalb der eigenen Domäne)

<br>

<h4>Namensschema: </h4>

- Benutzer: v.nachname (v = erster Buchstabe Vorname)
- Administratoren: Admin.Initialen (Initialen = Erster Buchstabe von Vorname und Nachname)
- Service: Service.Servicename (Servicename = Name des Dienstes / Fernzugreifer als Anbieter)
- Shared Mailbox: Shared.Mail.Postfach (Postfach = Name des Postfaches z.B. Info)

<h4>Folgende Identitäten werden erstellt:</h4>

- Benutzer: 
    - c.marosi
    - u.test (u = User)

- Administratoren: 
    - Admin.CM
    - Admin.T1 (T1 = Test 1)

- Service: 
    - 3CX.Admin
    - 3CX.User
    - TV.Admin
    - TV.User

- Shared Mailbox
    - Shared.Mail.IT-Dienste
    - Shared.Mail.Helpdesk
    - Shared.Mail.Webmaster

<br>

<h3> <b>Rollen Gruppen (Global Groups) </h3></b>

<h4>Definition:</h4>

Das sind die Mitglieder von lokalen Domänen Gruppen, welche eine Innen-Verschachtelung haben und ihre Identitäten mit den ähnlichen Berechtigungen auf ihre Rollen basieren für die Berechtigungen.

<h4>Namensschema: </h4>

RG = Rollen Gruppe

IT/... = Abteilung

TL/... = Position in Abteilung

Folgende Gruppen werden demnach erstellt:

- RG_IT
    - _TL
    - _Member
    
TL = TeamLeiter

<br>

- RG_HR (HR = Human Ressources)
    - _Team

<br>

- RG_GL_Member (GL = Geschäftsleitung)

<br>

- RG_Acc (Acc = Accounting)
    - _Team
    - _TL

<h4> Somit werden folgende Rollengruppen erstellt: </h4>

- RG_IT_TL
- RG_IT_Member
- RG_HR_Team
- RG_GL_Member
- RG_Acc_Team
- RG_Acc_TL

<h3> <b>Zugriffs Gruppen (Local Groups) </h3></b>

<h4>Definition</h4>

Die Lokale Domänen-Gruppen gewähren die Zugriffe auf die Ressourcen anhand der Domänen-Richtlinie basierend.

<h4>Namensschema: </h4>

Hierbei wird folgende Namenskonvention benutzt (das meiste mit 1 - 2 Buchstaben):

Erster Teil der Abkürzung gibt die Art des Zugriffes vor:
- DZ = Drucker Zugriff
- EZ = Exchange Zugriff
- PZ = Programm Zugriff
- SP = SharePoint Zugriff
    
Der zweite Teil der Abkürzung beschreibt den Untertyp, auf was Zugegriffen wird (Sei das Programm, Dienst, etc.).

Programme: 

- 3CX (PZ_3CX_)
- TeamViewer (PZ_TV_)

Mailbox:

- Vert (= Verteiler) -> EZ_Vert_
- Res (= Reservationen) -> EZ_Res_

Drucker: 

- DZ
    - DZ_ZH_

Sharepoint: 

- Pfad (SP_"SharePoint"__Pfad)

Der dritte Teil der Abkürzung beschreibt bei den Zugriffsgruppen, auf was zugegriffen wird oder als welches Level: 

Mailbox / Programme: 
- Member
- Leiter
- Admin

Ordner Zugriff / SharePoint: 
- R (Read)
- M (Modify)
- O (Owner)


Druckername: 
- PRTxxx (xx = Zahl von 01 beginnend)

Folgende Gruppen werden somit erstellt: 

- DZ_ZH_PRT001
- PZ_3CX_Admin
- PZ_3CX_Member
- PZ_TV_Admin
- PZ_TV_Member
- EZ_Res_Meeting
- EZ_Vert_Postfach