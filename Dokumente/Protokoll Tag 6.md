# Mittwoch, 17. Mai 2023
## Was für Arbeiten habe ich gemacht?

An diesem Tag ist bei mir vor allem an der Zeitplanung der Fokus gelegen. Mir ist als Rückmeldung gegeben worden dass das eben noch nicht hat. Also habe ich darauf gehört und mich sofort begonnen daran zu arbeiten können. Immerhin habe ich somit auch was gutes rausarbeiten können für mich selbst. Somit ist für jetzt das Script finalisieren und auch dann mit der Dokumentation zu beginnen zum sehen wie ich was gemacht habe. Ich bin schon am überlegen, statt immer "Tag x" auf "Woche x" umbennen aber wird kein Sinn ergeben je nach dem bei der Überschrift.

## Wie ist er mir ergangen?
Mir ist die Erarbeitung von der Zeitplanung her besser gegangen als ich gedacht habe da ich ja im Einreichungsformular schon mal einen Anfang schrieb bis wann was gemacht werden sollte. Nun habe ich einfach noch mehr Details hingeschrieben in den Terminplan um auch für mich selbst was zum Orientieren haben können. Wenn ich bedenke dass ich am Anfang noch nicht so gute Gedanken hatte und wo ich jetzt stehe, tut mich das schon beeindrucken weil ja einiges an Wissen gelernt habe vor allem beim Script eben. <br> 
Es war halt immer folgender Testing Ablauf bei mir: <br>
- Befehl suchen
- Wenn Befehl gefunden, wichtigste Präfixe hinzufügen
- Wenn kein Erfolg gewesen, dann schauen wo lag der Fehler und ausbessern
- Wenn erfolgreich gewesen ist, überprüfung in Azure

Sobald ich dann im Script die Finalisierung habe und alles auch wie gewollt Erfolgreich durchgeht ohne Fehlermeldungen, wird dann dokumentiert in einem eigenen Dokument.