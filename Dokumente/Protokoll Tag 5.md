# Donnerstag, 11. Mai 2023

## Was für Arbeiten habe ich gemacht?

Heute habe ich herausgefunden, wie in Azure die verschiedenen Zugriffsgruppen erstellen kann. Somit kann ich einen nehmen als Grundlagen für die anderen Gruppen und anpassen im Namen. Ebenfalls habe ich herausgefunden wie man neue Benutzer erstellt im lokalen PowerShell für die Azure Active Directory und in das Script integriert. Auch habe ich unterdessen getestet ob auch das Script wie gewünscht Abläuft nach Plan alles.

Der Befehl ist wie folgt für Azure User: New-AzADUser -DisplayName "" -MailNickname "" -UserPrincipalName "@christianmarosioutlookcom.onmicrosoft.com"

Der Befehl ist wie folgt für Azure Gruppen: New-AzADGroup -Description "" -DisplayName "" -MailNickName ""

### Erklärungen: 

- New-AzADUser: Erstellt einen neuen User im Azure Active Direcotry
- New-AzADGroup: Erstellt eine neue Azure Active Direcotry Gruppe
- -Description: Eine Beschreibung einfügen in die Gruppe (Für Erklärungen zum Beispiel)
- -Displayname: Zeigt an, wie der Gruppenname im Azure erscheinen sollte bei der Namensspalte
- -MailNickname: Das ist der E-Mailalias der Gruppe
- -UserPrincipalName: User der Benutzerprinzipialname (UPN) wo sich aus "Username@Domainname" ergibt für die Anmeldung zum Beispiel an einem Computer

## Wie ist er mir ergangen?

Ich bin sehr froh drüber, dass ich herausgefunden habe was ich schon mal als ein Teil des Erfolges bezeichnen kann. Dank das kann ich nun somit weiterfahren und endlich mal die Grundlagen erarbeiten.