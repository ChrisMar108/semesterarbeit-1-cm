# Projekt Dokumentation

## Wofür ist dieses Dokument gedacht?

Dieses Dokument ist die richtige Dokumentation, wie diese Arbeit entstanden ist und wie was umgesetzt wurde.

## Was für Material ist hierbei benötigt?

- Ein Microsoft Konto, welcher Verknüpft wird mit Microsoft Azure
- Microsoft PowerShell 7 lokal installiert
- Microsoft's Auzre PowerShell Lokal installiert
- Azure P2 Administrator
- Script Scope Bereich auf den Aktuellen Benutzer angepasst per Befehl

## Wie habe ich das ganze vorbereitet?

Als erstes muss gesagt werden, dass mein privates Microsoft Konto, welchen ich seit mindestens 8 Jahren schon bei mir habe dass der schon während meiner Lehre als ICT-Fachmann EFZ verknüpfte in das Azure um kostenlose Software herunterladen zu können.

Nachdem ich das gemacht hatte, bin ich auf die [Azure PowerShell](https://learn.microsoft.com/de-ch/powershell/azure/install-azps-windows?view=azps-10.0.0&tabs=windowspowershell&pivots=windows-psgallery) Seite zum Schritt für Schritt zu installieren.  

Nachdem PowerShell 7 Installiert wurde [(Hierbei nahm ich für saubere Installation das MSI Paket)](https://learn.microsoft.com/de-de/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.3), musste in der Windows 11-Terminal App (Standardmässige Konsole mittlerweile) PowerShell eingebunden werden. ![](../Screenshots/Terminal-Einstellungen%20PowerShell%207.png).

Nachdem der Adminstration von PowerShell 7 erfolgt ist, ging es an das nächste: <br>
1. Als ersten Schritt musste geschaut werden, welche PowerShell Version der 7 installiert ist. Das wird mit `$PSVersionTable.PSVersion` ermittelt. In meinem Fall zeigt das Majov Version 7, Minor Version 3 und Patch 4 an. Somit ist das die aktuellste Version 7.4.3
2. Als zweites muss ermittelt werden, wie die Execution Policy eingestellt ist auf dem Gerät mit PowerShell. Das wird mit `Get-ExecutionPolicy -List` ermittelt. In meinem Fall zeigte es bereits "RemoteSigned" bei CurrentUser (Aktueller Benutzer) sowie LocalMachine (Lokales Gerät) weil ich das mit dem nächsten Befehl `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser` so einstellte dass es auf den aktuellen Benutzer sowie dem Computer erlaubt, Skripts sowie andere PowerShell Befehle zu ausführen.
3. Sobald das Umgestellt wurde, und auf dem Gerät Azure PowerShell noch nicht installiert ist wird folgender Befehl genutzt: `Install-Module -Name Az -Repository PSGallery -Force`. Hierbei ist wichtig zu beachten, dass es einige Minuten dauern kann weil ja das ganze Modul herunterlädt. ![](../Screenshots/Az%20Module%20Update.png)
4. Da ich bereits heruntergeladen hatte, führte ich einen `Update-Module -Name Az -Force` aus um auf Aktualisierungen zu prüfen. 
5. Sobald das durchgegangen ist, müssen 2 Befehle durchgeführt werden zur Anmeldung: 
    - Connect-AzAccount --> Um den Account zu verbinden
    - Login-AzAccount --> Um mit dem eigenen Account sich zu anmelden
![](../Screenshots/Az-Account%20Login.png)

Nach diesen ganzen Schritten ist man nun noch nicht ganz bereit zu Scripten.

Ich habe online das eigene Microsoft Konto noch zu einem P2 Administrator aktivieren müssen, weil je nach Funktionen auch der P2 Admin benötigt wird. Hierbei habe ich eine eigene Kreditkarte hinterlegt und somit konnte das ganze beginnen.

## Dokumentation Namenskonvention

### Wie entstand die Namensgebung, welcher Verwendet wird?

#### <b>Bei den Identitäten setzt es sich wie folgt zusammen:</b> 

- Benutzer: v.nachname (v = erster Buchstabe Vorname)
- Administratoren: Admin.Initialen (Initialen = Erster Buchstabe von Vorname und Nachname)
- Service: Service.Servicename (Servicename = Name des Dienstes / Fernzugreifer als Anbieter)
- Shared Mailbox: Shared.Mail.Postfach (Postfach = Name des Postfaches z.B. Info)

#### <b>Somit entstanden folgende Identiäten: </b>

- Benutzer: 
    - c.marosi
    - u.test (u = User)

- Administratoren: 
    - Admin.CM
    - Admin.T1 (T1 = Test 1)

- Service: 
    - 3CX.Admin
    - 3CX.User
    - TV.Admin
    - TV.User

- Shared Mailbox
    - Shared.Mail.IT-Dienste
    - Shared.Mail.Helpdesk
    - Shared.Mail.Webmaster

#### <b>Bei den Rollengruppen wurde wie folgt umgesetzt: </b>
- RG = Rollen Gruppe -> das kommt vor jede Gruppe
- IT/... = Abteilung
- TL/... = Position der Abteilung

<h4> <b>Somit werden folgende Rollengruppen erstellt: </b></h4>

- RG_IT_TL
- RG_IT_Member
- RG_HR_Team
- RG_GL_Member
- RG_Acc_Team
- RG_Acc_TL

#### <b>Und die Zugriffs-Gruppen wurden in dieser Namensgebung definiert: </b> 
- Erste 1-2 Buchstaben sind die Art des Zugriffes
- Der Zweite Teil der Abkürzung beschreibt den Untertyp auf was Zugegriffen wird (Programm, Dienst, etc.)
- Der Dritte Teil der Abkürzung dafür ist das Zugriffslevel oder auf was zugegriffen wird

#### <b>Folgende Zugriffs-Gruppen wurden erstellt: </b>
- DZ_ZH_PRT001
- PZ_3CX_Admin
- PZ_3CX_Member
- PZ_TV_Admin
- PZ_TV_Member
- EZ_Res_Meeting
- EZ_Vert_Postfach