# Montag, 24. April 2023 + Dienstag, 25 April 2023

## Was für Arbeiten habe ich gemacht?
Ich habe vor allem Recherche Betrieben zum Herausfinden, wie ich zum Beispiel in Azure PowerShell eine neue Ressourcengruppe erstelle sowie die Verwaltung der Ressourcengruppe mache.

Ebenfalls habe ich mir die Namenskonvention überlegt, wie was benannt werden sollte. Mir ist die Idee mit der Ergänzung schlussendlich Dienstag, am 25. April 2023 zur beendigung in den Sinn gekommen wie ich das lösen kann.

Zwischen dem Mittwoch 26. April und Freitag 28. April habe ich dafür eine stärkere Überarbeitung gemacht in der Namenskonvention und mir ist somit besseres gelungen als ich gedacht habe.

## Wie ist er mir ergangen?
Ich habe ein wenig leider Schwierigkeiten gehabt für den Anfang, was für eine Namenskonvention ich benutzen sollte. Immerhin ist mir dann was einfachen in den Sinn gekommen, was auch recht passend sein wird und einfach zu ersetzen von den Namen her.

Zuerst habe ich echt nicht so richtig gemacht das gesamte an der Namenskonvention. Aber nachdem ich mit meinem Vorgesetzten mal den ersten Stand zusammen konnten, stellte sich heraus dass ich nicht so richtig war. Da haben wir dann 1.5 Stunden gemeinsam besprochen und im Anschluss hat es mir dann wirklich mehr Klarheit gegeben.