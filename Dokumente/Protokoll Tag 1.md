# Freitag, 21. April 2023

## Was für Arbeiten habe ich gemacht?

An diesem Tag habe ich zuerst auf meinem Gerät, welches ich für die HF Weiterbildung benutze. Mir sind am Donnerstagabend von Thanam 3 Links gesendet worden, welche mir auch sehr Hilfreich sein werden für die Semesterarbeit. 

[Link 1 mit Group Management](https://learn.microsoft.com/en-us/azure/active-directory/fundamentals/concept-learn-about-groups#group-types)

[Link 2 mit Azure Active Direcotry Pricing](https://www.microsoft.com/en-us/security/business/identity-access/azure-active-directory-pricing)

[Link 3 mit PowerShell Graph](https://learn.microsoft.com/en-us/powershell/microsoftgraph/overview?view=graph-powershell-1.0)

[Installations-Anleitung von Microsoft](https://learn.microsoft.com/de-ch/powershell/microsoftgraph/installation?view=graph-powershell-1.0)

Nachdem ich mir anschaute, wie man PowerShell Graph installiert habe ich bei mir Powershell 7 installiert und bei der Registry unter ` HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full ` überprüft welche .Net Framework Version ich habe. Es war die ![Version 4.8 (Siehe Screenshot)](../Screenshots/Net%20Framework%20Version.png)

Anschliessend ist mit `Install-Module Microsoft.Graph -Scope CurrentUser` dann der Microsoft Graph installiert worden.

Sobald all diese Vorbereitungen getroffen worden sind und mit meinem Privaten Microsoft Konto anmeldete auf Azure Portal sah ich beim Azure Active Directory nur ein "Azure AD Free" Account. Da mir gesagt worden ist dass ich unbedingt P2 brauche, habe ich natürlich auf das gepugraded (Die ersten 30 Tage sind sogar kostenlos). Mir ist das immerhin mit einer eigenen Kreditkarte gelungen. ![auf Azure AD Premium P2 zu sein.](../Screenshots/Azure-P2%20MS%20Account.png)
Somit kann ich dann während der Arbeit hauptsächlich auch dann darauf achten, dass ich Fortschritte machen kann. 

Als nächste Entscheidung war für mich ein wenig schwerer, wo dass ich dokumentieren sollte für den ersten Tag bereits als Anfang.

Meine Entscheidung ist aus folgenden Gründen auf GitLab Repository gefallen: 
- Eigene Struktur mit den Dateien sowie freie gestaltung
- Sehr einfache Gestaltung als Webseite für die eigenen Dokumentationen
- Jederzeit als Link abrufbar (Auch später bei Bewerungsunterlagen zum Beispiel)

## Wie ist es mir ergangen?
Am schwersten war es für mich zuerst bei Microsoft von einem "Azure AD Free" auf einen "Azure AD Premium P2" zu kommen. Sobald aber ich das geschafft habe und auch die Vorbereitungen lokal auf meinem eigenen Gerät installiert habe, kommen jetzt die nächsten Schritte. 

Diese Nächsten Schritte sind wie folgt: 
- Sinnvolle Namenskonvention benutzen
- Script sauber entwerfen und aufbauen
- Test-User und Test-Ressourcen im Azure erstellen und im Anschluss auf PowerShell-Script einbinden 