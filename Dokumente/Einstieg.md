# Ausgangslage beim Kunden

## Wofür ist dieses Dokument gedacht?

Dieses Dokument dient der aktuellen Ausgangslage beim Kunden zum eine Übersicht verschaffen, wie die Momentane Lage ist. Auch wird erläutert, was gemacht wird als die beste Lösung.

## Problemstellung
Im Moment ist kein sauberes Berechtigungskonzept vorhanden. Die Berechtigungen (Permissions) werden zurzeit direkt auf die Benutzer oder Maschinen zugewiesen auf einen Lokalen On-Prem Server. Hierbei ist ein erheblicher Aufwand verbunden, da die Änderungen auf dem lokalen AD-Server direkt gemacht werden müssen. Auch sind somit die einzelnen Benutzer direkt auf dem Server zu berechtigen, falls die Anforderungen nicht in das Momentane Konzept passen. Die Überprüfung ist auch sehr schwierig somit, da alles sehr verteilt über mehrere AD-Server, Unterordner oder Dienste eingerichtet wird.

## Ziele

Das Ziel ist es, dass die gesamte Benutzer-Administraton vereinfacht werden sollte. Auch soll ein einfacheres System geschaffen werden, welcher für die Zukunft einfachere Verwaltung beinhaltet und auch viel einfacher ausgebaut werden kann. Hierbei soll dem Management ermöglicht werden, die zuverlässig bestehende Zugriffe zu überprüfen. <br>
Hierbei wird nach dem Microsoft state-of-the-art IAM (Identity Acccess Management) - Prinzip umgesetzt.

Zum Einsatz kommt das Konzept IGDLA, was von ITIL empfohlen wird. 

Erklärung von IGDLA: 
-	I:    Identities, Users and Computers (Identitäten)
-	G:  Global Groups (basierend auf Rollen)
-	DL: Domain-Local Groups (Ressourcen Zugriff)
-	A:   Access (Ressourcen zuweisen)

Es wird eine universelle Namenskonvention benötigt. Dies schafft eine einheitliche Übersicht, welche ermöglicht verschiedene Gruppen zu bestimmen für die Berechtigungen. <br>

Den Identitäten werden Rollen zugewiesen, welchen wiederum die Zugriffs-Gruppen zugewiesen werden. Die Zugriffs-Gruppen werden den Ressourcen zugewiesen. <br>

Einzelne Identitäten können direkt eine Zugriffs-Gruppe erhalten, wenn benötigt, ohne dass die Übersicht verloren geht. <br>

Über ein PowerShell-Script werden die Zugriffs-Gruppen erstellt und den Ressourcen zugewiesen. Die neu erstellten Zugriffs-Gruppen werden dann manuell gemäss Vorgaben den Rollen-Gruppen zugewiesen. <br>

Die bestehenden (alten) Zuweisungen werden nach mehreren Tagen und Überprüfung der neuen Zuweisungen mittels PowerShell-Scripts entfernt. <br>

Beide PowerShell-Scripts werden mit Test-Users und Test-Ressourcen (Storage Account, App Service, usw.) getestet, bevor dies produktiv angewendet wird. <br>

Die weitere Administration von Zugriffen erfolgt über das Active Directory oder AAD (Azure Active Directory) im regulären Request-Management Prozess. <br>