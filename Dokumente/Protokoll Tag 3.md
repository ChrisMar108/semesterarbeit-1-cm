# Dienstag, 2. Mai 2023

## Was für Arbeiten habe ich gemacht?

An diesem Tag sind vor allem Ergänzungen gemacht worden bei der Namenskonvention. Auch sind dort die Beispiele zum Darstellen der Namenskonvention erstellt worden. Auch habe ich bei mir nun das "AZ" Modul via PowerShell installiert und abgewartet bis es vollständig durch war. Anhand davon habe ich nun endlich zum Beispiel die Ressourcen Gruppe erstellen können. Nun kann endlich mal an das Scripten gearbeitet werden und somit auch getestet sowie erster Entwurf vom Script erarbeitet.

## Wie ist er mir ergangen?

Ich habe am Anfang eine kleine Unübersichtlicheit gehabt weil ich nicht wusste ob ich eigene Struktur oder die des Kunden erstellen sollte. Immerhin ergab sich dann mit genauer anschauen ein viel besseres Bild für mich und somit konnte ich wieder richtig eingegleist werden. Immerhin ist mit auch recht gut geholfen worden, was ich auch loben muss. 