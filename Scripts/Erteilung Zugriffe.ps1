﻿# Hiermit meldet man sich auf den Azure Account an
Login-AzAccount

# Hiermit wird der Azure Account verbunden mit einer Anmeldung
Connect-AzAccount

# Hierbei wird ein Passwort Generiert, welches dann dem Azure Active Directory User (AAD) zugewiesen wird
function Get-RandomCharacters($length, $characters) {
    $random = 1..$length | ForEach-Object { Get-Random -Maximum $characters.length }
    $private:ofs=""
    return [String]$characters[$random]
}

function T1 ([string]$inputString){     
    $characterArray = $inputString.ToCharArray()   
    $scrambledStringArray = $characterArray | Get-Random -Count $characterArray.Length     
    $outputString = -join $scrambledStringArray
    return $outputString 

}

$password = Get-RandomCharacters -length 6 -characters 'abcdefghiklmnoprstuvwxyz'
$password += Get-RandomCharacters -length 2 -characters 'ABCDEFGHKLMNOPRSTUVWXYZ'
$password += Get-RandomCharacters -length 3 -characters '1234567890'
$password += Get-RandomCharacters -length 4 -characters '!§$%&/()=?}][{@#*+'

Write-Output $password

$securepassword = ConvertTo-SecureString -String $password -AsPlainText -Force 

Remove-Variable password

#Erstellt neue Benutzer, bei der das Passwort vom vorherigen Teil mit -Passwort zugewiesen wird
New-AzADUser -DisplayName "Christian Marosi" -MailNickname "c.marosi" -UserPrincipalName "c.marosi@christianmarosioutlookcom.onmicrosoft.com" -Password $securepassword
New-AzADUser -DisplayName "User Test" -MailNickname "u.test" -UserPrincipalName "u.test@christianmarosioutlookcom.onmicrosoft.com" -Password $securepassword
New-AzADUser -DisplayName "Admin Christian Marosi" -MailNickname "Admin.CM" -UserPrincipalName "Admin.CM@christianmarosioutlookcom.onmicrosoft.com" 
New-AzADUser -DisplayName "Admin Test 1" -MailNickname "Admin.T1" -UserPrincipalName "Admin.T1@christianmarosioutlookcom.onmicrosoft.com" 
New-AzADUser -DisplayName "3CX Administrator" -MailNickname "3CX.Admin" -UserPrincipalName "3CX.Admin@christianmarosioutlookcom.onmicrosoft.com" 
New-AzADUser -DisplayName "3CX User" -MailNickname "3CX.User" -UserPrincipalName "3CX.User@christianmarosioutlookcom.onmicrosoft.com" 
New-AzADUser -DisplayName "TeamViewer Administrator" -MailNickname "TV.Admin" -UserPrincipalName "TV.Admin@christianmarosioutlookcom.onmicrosoft.com" 
New-AzADUser -DisplayName "TeamViewer User" -MailNickname "TV.User" -UserPrincipalName "TV.User@christianmarosioutlookcom.onmicrosoft.com" 
New-AzADUser -DisplayName "Shared Mailbox IT-Dienste" -MailNickname "Shared.Mail.IT-Dienste" -UserPrincipalName "Shared.Mail.IT-Dienste@christianmarosioutlookcom.onmicrosoft.com" 
New-AzADUser -DisplayName "Shared Mailbox Helpdesk" -MailNickname "Shared.Mail.Helpdesk" -UserPrincipalName "Shared.Mail.Helpdesk@christianmarosioutlookcom.onmicrosoft.com" 
New-AzADUser -DisplayName "Shared Mailbox Webmaster" -MailNickname "Shared.Mail.Webmaster" -UserPrincipalName "Shared.Mail.Webmaster@christianmarosioutlookcom.onmicrosoft.com" 

#Erstellt die verschiedenen Zugriffs-Gruppen
New-AzADGroup -Description "Drucker Zugriff Zürich" -DisplayName "DZ_ZH_PRT001" -MailNickName "DZ_ZH_PRT001"
New-AzADGroup -Description "Programm Zugriff 3CX Administrator" -DisplayName "PZ_3CX_Admin" -MailNickName "PZ_3CX_Admin"
New-AzADGroup -Description "Programm Zugriff 3CX Mitglied (Kann Telefon benutzen)" -DisplayName "PZ_3CX_Member" -MailNickName "PZ_3CX_Member"
New-AzADGroup -Description "Programm Zugriff TeamViewer Administrator" -DisplayName "PZ_TV_Admin" -MailNickName "PZ_TV_Admin"
New-AzADGroup -Description "Programm Zugriff TeamViewer Mitglied" -DisplayName "PZ_TV_Member" -MailNickName "PZ_TV_Member"
New-AzADGroup -Description "Exchange Zugriff Ressource Meeting" -DisplayName "EZ_Res_Meeting" -MailNickName "EZ_Res_Meeting"
New-AzADGroup -Description "Exchange Zugriff Verteiler Postfach" -DisplayName "EZ_Vert_Postfach" -MailNickName "EZ_Vert_Postfach"

# Endresultat zeigen ob Anmeldung erfolgreich verlaufen ist
Write-Output